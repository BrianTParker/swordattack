/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swordattack;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Client;
import java.io.IOException;
import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Brian.Parker
 */
public class PlaySwordAttack {
    
    Scanner scanner;
    private boolean host, play;
    String name;
    List<Player> allPlayers = new ArrayList<Player>();
    Player currentPlayer;
    int numPlayers;
    String IP;
    boolean has_players = false;
    
    
    SwordClient client;
    SwordServer server;
    String[] blockOptions;
    String[] attackOptions;
    boolean proceed;
    public TurnInfo turn;
    int target;
    int block;
    int attack;
    
    public PlaySwordAttack(){
        
        scanner = new Scanner(System.in);
        host = false;
        play = true;
        numPlayers = 0;
        blockOptions = new String[]{"High", "Low", "Full","Nothing"};
        attackOptions = new String[]{"High", "Low", "Full High","Full Low", "Nothing"};
        proceed = false;
        target = -1;
        block = -1;
        attack = -1;
        
        
    }
    
    
    public void play() throws IOException{
        
        
        String menuOption;
        IP = "";
        
        System.out.println("What would you like to do?");
        System.out.println("1.Host Game");
        System.out.println("2.Join Game");
        System.out.println("3.Exit");
        
        menuOption = scanner.next();
        
        while(!menuOption.equals("1") && !menuOption.equals("2") && !menuOption.equals("3")){
            
            System.out.println("That was not a valid selection.  Please try again");
            System.out.println("What would you like to do?");
            System.out.println("1.Host Game");
            System.out.println("2.Join Game");
            System.out.println("3.Exit");
        
        menuOption = scanner.next();
        }
        if(menuOption.equals("1")){
            
            host = true;  
            IP = "localhost";
            
        }else if(menuOption.equals("2")){
        	clearConsole();
            System.out.println("Please enter the IP address");
            IP = scanner.next();
            
            
        }else if(menuOption.equals("3")){
            System.exit(0);
        }
        
        clearConsole();
        
        System.out.println("Please enter your name");
        name = scanner.next();
        currentPlayer = new Player(name);
        allPlayers.add(currentPlayer);
        if(host){
        	
        	createServer();
        			
        	clearConsole();
            System.out.println("How many players are going to play?");
            numPlayers = scanner.nextInt();
            server.setNumPlayers(numPlayers);

        }
        
        
    	createClient(IP);
    			
        
        client.sendConnectionMessage(name);
        
        
        clearConsole();
        
        new Thread(){    
        	
        	public void run(){
        		this.setName("Logic Thread");
        			while(true){
        				client.setNextRound(false);
			            if(client.checkGameReady() == false){
			                System.out.println("Waiting for more players");
			            }
			            
			            while(client.getNumPlayers() == 0){
			            	try{
			                    Thread.sleep(1000);
			                }catch(InterruptedException e){
			                    e.printStackTrace();
			                }
			            	client.pingForNumPlayers();
			            }
			            
			            while(client.getPlayerNames() == null){
			                try{
			                    Thread.sleep(1000);
			                }catch(InterruptedException e){
			                    e.printStackTrace();
			                }
			                client.pingForPlayerList();
			                try{
			                	client.updateClient();
			                }catch(IOException e){
			                	e.printStackTrace();
			                }
			                
			            }
			            while(client.getPlayerNames().length != client.getNumPlayers()){
			            	
			            	
			                try{
			                    Thread.sleep(1000);
			                }catch(InterruptedException e){
			                    e.printStackTrace();
			                }
			                client.pingForPlayerList();
			                try{
			                	client.updateClient();
			                }catch(IOException e){
			                	e.printStackTrace();
			                }
			            }
			            if(client.getPlayerNames() != null){
			            	if(currentPlayer.getIsDead() == false){
				                clearConsole();
				                for(int i = 0; i < client.getPlayerNames().length; i++){
				                    System.out.println(i+1 + ": " + client.getPlayerNames()[i] + " HP: " + client.getPlayerHealth()[i]);
				
				                }
				                target = -1;
				                block = -1;
				                attack = -1;
				
				                System.out.println("Who would you like to attack?");
				                while(target == -1){
				                    target = scanner.nextInt();
				                    while(target < 0 && target > client.getPlayerNames().length){
				
				                        System.out.println("That was not a valid selection");
				                        System.out.println("Please try again");
				                        target = scanner.nextInt();
				
				
				                    }
				                }
				
				                clearConsole();
				
				                System.out.println("How you you like to Attack?");
				                System.out.println("1. High");
				                System.out.println("2. Low");
				                System.out.println("3. Full High");
				                System.out.println("4. Full Low");
				                System.out.println("5. Do nothing");
				                
				                while(attack == -1){
				                		                
				                    attack = scanner.nextInt();
				                    while(attack < 1 && attack > 5){
				                        System.out.println("That was not a valid selection");
				                        System.out.println("Please try again");
				                        System.out.print(">");
				                        attack = scanner.nextInt();
				                    }
				                }
				                
				                
				                clearConsole();
				
				                System.out.println("How you you like to block?");
				                System.out.println("1. High");
				                System.out.println("2. Low");
				                System.out.println("3. Full");
				                System.out.println("4. Do Nothing");
				                while(block == -1){
				                    block = scanner.nextInt();
				                    while(block < 0 && block > 4){
				                        System.out.println("That was not a valid selection");
				                        System.out.println("Please try again");
				                        block = scanner.nextInt();
				                    }
				                }
				                while((attack == 3 || attack == 4) && block != 4){
				                    System.out.println("You can't block if you did a full attack.");
				                    System.out.println("Please make another selection.");
				                    block = scanner.nextInt();
				                }
				                while((block == 3) && attack != 5){
				                    System.out.println("You can't full block if you are also attacking");
				                    System.out.println("Please make another selection");
				                    block = scanner.nextInt();
				                }
				               
				
				                turn = new TurnInfo();
				                turn.attack = attackOptions[attack - 1];
				                turn.block = blockOptions[block - 1];
				                turn.target = client.getPlayerNames()[target - 1];
				                turn.sender = currentPlayer.getName();
			            	}else{
			            		turn = new TurnInfo();
			            		turn.attack = "dead";
			            		turn.block = "dead";
			            		turn.target = "dead";
			            		turn.sender = currentPlayer.getName();
			            	}
			                client.sendTurnInfo(turn);
			                clearConsole();
			                System.out.println("Waiting for other players to finish...");
			                
			                
			                while(client.getProceed() == false){
			                	try {
									Thread.sleep(2000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
			                	
			                	if(client.getProceed() == false){
			                		
			                		client.pingForResults();
			                	}
				                
				                
				                
				                
			                }
			                
			                
			                client.setProceed(false);
			                clearConsole();
			                client.displayRoundResults(currentPlayer);
			                System.out.println("\n");
			                
			                String winner = client.checkForWinner();
			                if(!winner.equals("continue")){
			                	if(winner.equals("draw")){
			                		System.out.println("Everyone is dead!");
			                		System.out.println("The game is a draw.");
			                	}else{
			                		System.out.println(winner + " is the winner!");
			                		
			                	}
			                	
			                	System.out.println("\n");
				                System.out.println("Press Enter to exit the game.");
				                try{System.in.read();}
				                catch(Exception e){
				                }finally{
				                	
				                }
				                
				                System.exit(0);
			                }else{
			                	
			                	
			                	System.out.println("Press Enter when you're ready");
				                try{System.in.read();}
				                catch(Exception e){
				                }
				                
				                client.endTurn();
				                
				                if(client.getNextRound() == false){
				                	System.out.println("Waiting for other players to hit ready");
				                	
				                }
				
				                while(client.getNextRound() == false){
				                	try {
										Thread.sleep(2000);
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
				                	
				                	if(client.getNextRound() == false){
				                		client.pingForNextRound();
				                	}
				                }
				                
			                }
			
			            }
			
			        }
        	}
        }.start();
                
            
            
            
            
        
        
        
        
        
    }
    
    
    
    public void createServer() throws IOException{
        
        server = new SwordServer();
           
//        new Thread(new Runnable(){
//                public void run(){
//                    try{
//                        SwordServer server = new SwordServer();
//                    }catch(IOException e){
//                        e.printStackTrace();
//                    }
//                }
//                
//            }).start();
    }
    
    public void createClient(String ip) throws IOException{
        client = new SwordClient(ip);
    }
    
    
    private static void clearConsole()
    {
    	
    	for (int y = 0; y < 2; y++){
    		System.out.println("\n");
    	}
    }
    
    private void clearConsole2(){
    	for (int y = 0; y < 25; y++){
    		System.out.print("\b");
    	}
    }
    
}
