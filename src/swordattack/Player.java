/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swordattack;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.EndPoint;

/**
 *
 * @author Brian.Parker
 */

public class Player {
    
    private String name;
    private int health;
    private int money;
    private int id;
    private boolean isDead = false;
    EndPoint endPoint;
    Connection con;
    private String block, attack;
    
    public Player(String inName){
        name = inName;
        health = 5;
        money = 100;
    }
    
    public Player(int inId, EndPoint inEP){
        health = 5;
        money = 100;
        id = inId;
        endPoint = inEP;
    }
    
    public Player(Connection inConnection){
        health = 5;
        money = 100;
        con = inConnection;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String inName){
        name = inName;
    }
    
    public int getID(){
        return id;
    }
    
    public void setConnection(Connection inCon){
        con = inCon;
        
    }
    
    public Connection getConnection(){
        return con;
    }
    
    public void setBlock(String inBlock){
        block = inBlock;
    }
    
    public String getBlock(){
        return block;
    }
    
    public void setAttack(String inAttack){
        attack = inAttack;
    }
    
    public String getAttack(){
        return attack;
    }
    
    public void reduceHealth(int inDamage){
        health -= inDamage;
    }
    
    public void setHealth(int inHealth){
        health = inHealth;
    }
    
    public int getHealth(){
        return health;
    }
    
    public void setIsDead(boolean inDead){
    	isDead = inDead;
    }
    
    public boolean getIsDead(){
    	return isDead;
    }
    
}
