/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swordattack;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Client;
import java.io.IOException;
import com.esotericsoftware.minlog.Log;
import java.util.ArrayList;
import java.util.List;
/**
/**
 *
 * @author Brian.Parker
 */
public class SwordClient {
    
    private Client client;
    private boolean game_ready;
    private String[] nameList;
    private int[] healthList;
    List<Player> allPlayers = new ArrayList<Player>();
    private int numPlayers = 0;
    private boolean nextRound;
    private Engagement roundResults;
    private boolean proceed;
    private String winner;
    
    
    
    public SwordClient(String IP) throws IOException{
        client = new Client();
        game_ready = false;
        proceed = false;
        nextRound = false;
        registerPackets();
        new Thread(client).start();
        client.connect(600000, IP, 54555, 54777);
        
        
        
        client.addListener(new Listener(){
            
            public void disconnected(Connection connection){
            	try {
					client.reconnect();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
            @Override
            public void received(Connection connection, Object object){
                if(object instanceof SomeResponse){
                    SomeResponse response = (SomeResponse)object;
                    System.out.println(response.text);
                }else if(object instanceof ServerMessage){
                    ServerMessage message = (ServerMessage)object;
                   
                    System.out.println("Server Message: " + message.text);
                    
                }else if(object instanceof GameReady){
                    game_ready = true;
                }else if(object instanceof PlayerList){
                	
                    PlayerList list = (PlayerList)object;
                    nameList = list.nameList;
                    healthList = list.health;
                    
                }else if(object instanceof NumPlayers){
                    NumPlayers num = (NumPlayers)object;
                    numPlayers = num.numPlayers;
                }else if(object instanceof Engagement){
                	
                    Engagement results = (Engagement)object;
                    roundResults = results;
                    
                    proceed = true;
                    
                }else if(object instanceof EndTurn){
                	nextRound = true;
                }
            }

        });
        
    }
    
    
    private void registerPackets(){
        Kryo kryo = client.getKryo();
        kryo.register(SomeRequest.class);
        kryo.register(SomeResponse.class);
        kryo.register(NewConnection.class);
        kryo.register(ServerMessage.class);
        kryo.register(GameReady.class);
        kryo.register(TurnInfo.class);
        kryo.register(PlayerList.class);
        kryo.register(NewPlayer.class);
        kryo.register(String[].class);
        kryo.register(NumPlayers.class);
        kryo.register(Engagement.class);
        kryo.register(PingResults.class);
        kryo.register(int[].class);
        kryo.register(EndTurn.class);
        kryo.register(PingEndTurn.class);
    }
    
    public void sendRequest(){
        SomeRequest request = new SomeRequest();
        request.text = "Here is the request!";
        client.sendTCP(request);
    }
    
    public void sendConnectionMessage(String inString){
        NewConnection connect = new NewConnection();
        connect.text = inString;
        client.sendTCP(connect);
    }
    
    public void updateClient() throws IOException{
        client.update(60000);
    }
    
    public boolean checkGameReady(){
        return game_ready;
    }
    
    public Client getClient(){
        return client;
    }
    
    public String[] getPlayerNames(){
        return nameList;
    }
    
    public int[] getPlayerHealth(){
    	return healthList;
    }
    
    public void pingForPlayerList(){
        PlayerList list = new PlayerList();
        list.nameList = new String[1];
        list.health = new int[1];
        client.sendTCP(list);
    }
    
    public void pingForNumPlayers(){
    	NumPlayers num = new NumPlayers();
    	num.numPlayers = 0;
    	client.sendTCP(num);
    }
    
    public void pingForResults(){
    	PingResults ping = new PingResults();
    	ping.text = "ping";
    	client.sendTCP(ping);
    }
    
    public void pingForNextRound(){
    	PingEndTurn pingTurn = new PingEndTurn();
    	pingTurn.text = "ping";
    	client.sendTCP(pingTurn);
    	
    }
    
    public int getNumPlayers(){
        return numPlayers;
    }
    
    public void sendTurnInfo(TurnInfo turn){
        client.sendTCP(turn);
    }
    
    public boolean getProceed(){
        return proceed;
    }
    
    public void setProceed(boolean inProceed){
        proceed = inProceed;
    }
    
    public boolean clientIsConnected(){
    	return client.isConnected();
    }
    
    public void displayRoundResults(Player inPlayer){
        int damage;
        System.out.println("####################################################");
        
        for(int i = 0; i < roundResults.attackers.length; i++){
        	if(roundResults.attacks[i].equals("dead")){
        		System.out.println(roundResults.attackers[i] + " did nothing because he is dead");
        		System.out.println("\n");
        	}else{
	            System.out.println(roundResults.attackers[i] + " performed a " + roundResults.attacks[i] + " attack on " + roundResults.targets[i]);
	            System.out.println(roundResults.targets[i] + " blocked " + roundResults.blocks[i] + " and received " + roundResults.damage[i] + " damage");
	            
        	}
            
            if(inPlayer.getName().equals(roundResults.targets[i]) && roundResults.damage[i] > 0){
                inPlayer.reduceHealth(roundResults.damage[i]);
                if(inPlayer.getHealth() <= 0){
                    inPlayer.setHealth(0);
                    inPlayer.setIsDead(true);
                    
                }
            }
            
            for(int p = 0; p < nameList.length; p++){
            	
            	if(nameList[p].equals(roundResults.targets[i])  && roundResults.damage[i] > 0){
            		healthList[p] -= roundResults.damage[i];
            		if(healthList[p] <= 0){
            			healthList[p] = 0;
            			System.out.println(roundResults.targets[i] + " was slain by " + roundResults.attackers[i] + "!");
            			
            		}
            	}
            }
            System.out.print("\n");
            
        }
        
        System.out.println("####################################################");
        
    }
    
    public void clientReconnect() throws IOException{
    	client.reconnect();
    }
    
    public void endTurn(){
    	EndTurn end = new EndTurn();
    	end.text = "end";
    	client.sendTCP(end);
    	
    }
    
    public void setNextRound(boolean inBool){
    	nextRound = inBool;
    }
    
    public boolean getNextRound(){
    	return nextRound;
    }
    
    public String getWinner(){
    	return winner;
    }
    
    public String checkForWinner(){
    	int deathCount = 0;
    	String winner = "";
    	for(int i = 0; i < nameList.length; i++){
    		if(healthList[i] == 0){
    			deathCount++;
    		}else{
    			winner = nameList[i];
    		}
    	}
    	if(deathCount == nameList.length - 1){
    		return winner;
    	}else if(deathCount == nameList.length){
    		return "draw";
    	}else{
    		return "continue";
    	}
    }
    
}
