/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swordattack;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import java.io.IOException;
import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.kryonet.Server;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Brian.Parker
 */
public class SwordServer {
    
    private ServerMessage message = new ServerMessage();
    private Server server;
    private int endTurnCount;
    private int numPlayers;
    private int round;
    private Engagement resultList;
    private EngagementDetails[] resultsList;
    private List<Player> allPlayers = new ArrayList<Player>();
    private List<TurnInfo> playerTurns = new ArrayList<TurnInfo>();
    public SwordServer() throws IOException{
    	round = 1;
        server = new Server();
        endTurnCount = 0;
        new Thread(server).start();
        server.bind(54555, 54777);
        
        Kryo kryo = server.getKryo();
        kryo.register(SomeRequest.class);
        kryo.register(SomeResponse.class);
        kryo.register(NewConnection.class);
        kryo.register(ServerMessage.class);
        kryo.register(GameReady.class);
        kryo.register(TurnInfo.class);
        kryo.register(PlayerList.class);
        kryo.register(NewPlayer.class);
        kryo.register(String[].class);
        kryo.register(NumPlayers.class);
        kryo.register(Engagement.class);
        kryo.register(PingResults.class);
        kryo.register(int[].class);
        kryo.register(EndTurn.class);
        kryo.register(PingEndTurn.class);
        
        server.addListener(new Listener(){
        	
        	public void disconnected(Connection connection){
        		
        		System.out.println("Someone Disconnected");
        	}
            
            public void connected(Connection connection){
                allPlayers.add(new Player(connection.getID(), connection.getEndPoint()));
                
                NumPlayers num = new NumPlayers();
                num.numPlayers = numPlayers;
                connection.sendTCP(num);
                
                if(server.getConnections().length == numPlayers){
                    
                    
                    sendToAllClients(new GameReady("ready"));
                }
                
            }

            public void received(Connection connection, Object object){
                if(object instanceof SomeRequest){
                   
                    
                }else if(object instanceof NewConnection){
                    
                    NewConnection con = (NewConnection)object;
                    
                    
                    for(int i = 0; i < allPlayers.size(); i++){
                        
                        
                        if(connection.getID() == allPlayers.get(i).getID()){
                            allPlayers.get(i).setName(con.text);        
 
                        }
                        
                    }
 
                }else if(object instanceof PlayerList){
                	
                    String[] nameList = new String[allPlayers.size()];
                    int[] healthList = new int[allPlayers.size()];
                    for(int i = 0; i < allPlayers.size(); i++){
                        nameList[i] = allPlayers.get(i).getName();
                        healthList[i] = allPlayers.get(i).getHealth();
                    }
                    PlayerList listPacket = new PlayerList();
                    listPacket.nameList = nameList;
                    listPacket.health = healthList;
                    connection.sendTCP(listPacket);
                }else if(object instanceof TurnInfo){
                    TurnInfo playerTurn = (TurnInfo)object;
                    playerTurns.add(playerTurn);
                    
                    
                    
                    if(playerTurns.size() == allPlayers.size()){
                        processTurn(playerTurns);
                    }
                }else if(object instanceof NumPlayers){
                	NumPlayers num = new NumPlayers();
                    num.numPlayers = numPlayers;
                    connection.sendTCP(num);
                }else if(object instanceof PingResults){
                	
                	
                	if(resultList != null){
                		
                		
                        //results.turn = resultsList;
                		
                		
                        connection.sendTCP(resultList);
                        
                	}
                	
                }else if (object instanceof EndTurn){
                	
                	endTurnCount++;
                	
                	if(endTurnCount == allPlayers.size()){
                		endTurnCount = 0;
                		resultList = null;
                		playerTurns = new ArrayList<TurnInfo>();
                		round++;
                		EndTurn nextRound = new EndTurn();
                		nextRound.text = "end";
                		sendToAllClients(nextRound);
                	}
                	
                }else if (object instanceof PingEndTurn){
                	if(endTurnCount == allPlayers.size()){
                		EndTurn nextRound = new EndTurn();
                		nextRound.text = "end";
                		connection.sendTCP(nextRound);
                	}
                }
                
            }

        });
        
    }
    
    public void sendToAllClients(Object object){
        for(int p = 0; p < server.getConnections().length; p++){
            server.getConnections()[p].sendTCP(object);
        }

    }
    
    public void sendToAllClients(Object[] object){
        for(int p = 0; p < server.getConnections().length; p++){
            server.getConnections()[p].sendTCP(object);
        }

    }
    
    public void sendToAllOtherClients(Object object, int inId){
        for(int p = 0; p < server.getConnections().length; p++){
            if(server.getConnections()[p].getID() != inId){
                server.getConnections()[p].sendTCP(object);
            }
            
        }
    }
    public void setNumPlayers(int inNum){
        numPlayers = inNum;
    }
    
    public void processTurn(List<TurnInfo> inTurn){
    	
        resultList = new Engagement();
        resultList.attackers = new String[inTurn.size()];
        resultList.targets = new String[inTurn.size()];
        resultList.blocks = new String[inTurn.size()];
        resultList.attacks = new String[inTurn.size()];
        resultList.damage = new int[inTurn.size()];
        
        
        //get block and attack information
        for(int i = 0; i < inTurn.size(); i++){
           for(int p = 0; p < allPlayers.size(); p++){
               if(allPlayers.get(p).getName().equals(inTurn.get(i).sender)){
                   allPlayers.get(p).setBlock(inTurn.get(i).block);
                   allPlayers.get(p).setAttack(inTurn.get(i).attack);
                   
               }
           }
        }
        int damage = 0;
        
        
        for(int i = 0; i < inTurn.size(); i++){
        	
            for(int p = 0; p < allPlayers.size(); p++){
            	
                
                if(allPlayers.get(p).getName().equals(inTurn.get(i).target)){
                    damage = 0;
                    if(allPlayers.get(p).getBlock().equals("High") && inTurn.get(i).attack.equals("High")){
                        
                    }else if(allPlayers.get(p).getBlock().equals("High") && inTurn.get(i).attack.equals("Low")){
                        damage = 1;
                    }else if(allPlayers.get(p).getBlock().equals("Low") && inTurn.get(i).attack.equals("Low")){
                        
                    }else if(allPlayers.get(p).getBlock().equals("Low") && inTurn.get(i).attack.equals("High")){
                        damage = 1;
                    }else if (allPlayers.get(p).getBlock().equals("Low") && inTurn.get(i).attack.equals("Full High")){
                        damage = 2;
                    }else if (allPlayers.get(p).getBlock().equals("Low") && inTurn.get(i).attack.equals("Full Low")){
                        
                    }else if (allPlayers.get(p).getBlock().equals("High") && inTurn.get(i).attack.equals("Full High")){
                        
                    }else if (allPlayers.get(p).getBlock().equals("High") && inTurn.get(i).attack.equals("Full Low")){
                        damage = 2;
                    }else if (allPlayers.get(p).getBlock().equals("Full")){
                        
                    }else if(inTurn.get(i).attack.equals("High") || inTurn.get(i).attack.equals("Low")){
                        damage = 1;
                    }else if(inTurn.get(i).attack.equals("Full High") || inTurn.get(i).attack.equals("Full Low")){
                        damage = 2;
                    }
                    
                    if(damage > 0){
                        allPlayers.get(p).reduceHealth(damage);
                    }
                    
                    
                    resultList.attackers[i] = inTurn.get(i).sender;
                    resultList.attacks[i] = inTurn.get(i).attack;
                    resultList.targets[i] = inTurn.get(i).target;
                    
                    resultList.blocks[i] = allPlayers.get(p).getBlock();
                    resultList.damage[i] = damage;
                    
                    
        
                }else if(inTurn.get(i).target.equals("dead")){
                	resultList.attackers[i] = inTurn.get(i).sender;
                    resultList.attacks[i] = inTurn.get(i).attack;
                    resultList.targets[i] = inTurn.get(i).target;
                    
                    resultList.blocks[i] = allPlayers.get(p).getBlock();
                    resultList.damage[i] = damage;
                }
                
            }
            
        }
        
        
        
        
        
        
    }
    
    
}
